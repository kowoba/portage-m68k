# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7
inherit git-r3

DESCRIPTION="Macintosh m68k Bootloader"
HOMEPAGE="https://github.com/vivier/EMILE"
EGIT_REPO_URI="https://github.com/vivier/EMILE.git"
#SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
IUSE=""
KEYWORDS="~m68k"
DEPEND="app-text/docbook-sgml-utils"

RDEPEND=""

