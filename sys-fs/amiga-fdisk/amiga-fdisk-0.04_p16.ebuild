# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit toolchain-funcs flag-o-matic

DESCRIPTION="Create and edit RDB partition table of a disk partitioned in Amiga format"
HOMEPAGE="https://packages.qa.debian.org/a/amiga-fdisk.html"
SRC_URI="
	mirror://debian/pool/main/a/${PN}/${PN}_${PV/_p*}.orig.tar.gz
	mirror://debian/pool/main/a/${PN}/${PN}_${PV/_p*}-${PV/*_p}.debian.tar.xz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 -loong ~ppc ~ppc64 -riscv ~x86 ~m68k"

S=${WORKDIR}/${P/_p*}.orig


PATCHES=(
  "${WORKDIR}"/debian/patches/0001_readline
  "${WORKDIR}"/debian/patches/0002_amigastuff
  "${WORKDIR}"/debian/patches/0003_makefile
)

src_compile() {
	append-cflags -I./include -DDONT_USE_READLINE -DFIX_NO_READLINE
	emake CC="$(tc-getCC)"
}

src_install() {
	into /
	dosbin amiga-fdisk
	doman amiga-fdisk.8
	dodoc ChangeLog COPYING README ToDo ${WORKDIR}/debian/changelog
}
